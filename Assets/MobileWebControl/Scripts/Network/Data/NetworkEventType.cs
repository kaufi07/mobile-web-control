namespace MobileWebControl.Network.Data
{
    public enum NetworkEventType
    {
        Register_Player,
        Unregister_Player,
        Network_Input_Event
    }
}